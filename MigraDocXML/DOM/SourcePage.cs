﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML.DOM
{
	public class SourcePage : LogicalElement
	{
		public SourcePage()
		{
			IsLogical = false;
		}

		public override void Run(Action childProcessor)
		{
			var area = GetParentOfType<Area>();
			var page = area.GetParentOfType<Page>();
			var output = page.GetParentOfType<Output>();

			if (!output.PdfForms.ContainsKey(Document))
				throw new Exception("Could not find document of name " + Document);
			var sourceForm = output.PdfForms[Document];

			sourceForm.PageNumber = Page;
			page.GetXGraphics().DrawImage(sourceForm, area.GetXRect());
		}


		/// <summary>
		/// The name of the source document to pull from
		/// </summary>
		public string Document { get; set; }

		public int Page { get; set; }
	}
}
