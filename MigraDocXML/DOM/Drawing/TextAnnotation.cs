﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace MigraDocXML.DOM.Drawing
{
	public class TextAnnotation : GraphicalElement
	{
		public TextAnnotation()
		{
			NewVariable("TextAnnotation", this);
			ParentSet += TextAnnotation_ParentSet;
			FullyBuilt += TextAnnotation_FullyBuilt;
		}

		private void TextAnnotation_ParentSet(object sender, EventArgs e)
		{
			DOMRelations.Relate(GetGraphicalParent(), this);
			Page = GetGraphics()?.Page;
		}

		private void TextAnnotation_FullyBuilt(object sender, EventArgs e)
		{
			if (Page == null)
				throw new InvalidOperationException("Not specified which page to add annotation to");

			var pdfPage = GetDocument().GetPage(Page.Value);
			var canvas = GetDocument().GetPageCanvas(Page.Value);
			if (pdfPage == null || canvas == null)
				return;

			var annotation = new PdfTextAnnotation();
			annotation.Title = Title;
			annotation.Subject = Subject;
			annotation.Contents = Contents;
			annotation.Icon = _icon;
			if (_color != null)
				annotation.Color = _color.Value;
			annotation.Opacity = Opacity;
			annotation.Open = Open;
			var rect = canvas.Transformer.WorldToDefaultPage(new XRect(Left.Points, Top.Points, Width?.Points ?? 0, Height?.Points ?? 0));
			annotation.Rectangle = new PdfRectangle(rect);
			pdfPage.Annotations.Add(annotation);
		}


		public int? Page { get; set; }

		public string Title { get; set; }

		public string Subject { get; set; }

		public string Contents { get; set; }

		private PdfTextAnnotationIcon _icon;
		public string Icon
		{
			get => _icon.ToString();
			set => _icon = Parse.Enum<PdfTextAnnotationIcon>(value);
		}

		private XColor? _color = null;
		public string Color
		{
			get => _color?.ToString() ?? "";
			set => _color = Parse.XColor(value);
		}

		public double Opacity { get; set; } = 1.0;

		public bool Open { get; set; } = false;

		public Unit Left { get; set; }

		public Unit Top { get; set; }

		public Unit Width { get; set; }

		public Unit Right
		{
			get => Left + Width;
			set
			{
				if (value == null)
					throw new InvalidOperationException("Right attribute must be set to a non-null value");
				if (Left == null && Width == null)
					throw new InvalidOperationException("Cannot set Right attribute without setting either Left or Width first");
				if (Left != null)
					Width = value - Left;
				else
					Left = value - Width;
			}
		}

		public Unit Height { get; set; }

		public Unit Bottom
		{
			get => Top + Height;
			set
			{
				if (value == null)
					throw new InvalidOperationException("Bottom attribute must be set to a non-null value");
				if (Top == null && Height == null)
					throw new InvalidOperationException("Cannot set Bottom attribute without setting either Top or Height first");
				if (Top != null)
					Height = value - Top;
				else
					Top = value - Height;
			}
		}

		public RenderArea Area
		{
			set
			{
				Top = value?.Top;
				Left = value?.Left;
				Width = value?.Width;
				Height = value?.Height;
				Page = value?.Page;
			}
		}
	}
}
