﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{
    public abstract class ElementBase
    {
    }


    public abstract class OccursElement : ElementBase
    {
        public int? MinOccurs { get; private set; }

        public int? MaxOccurs { get; private set; }

        public OccursElement(int? minOccurs = null, int? maxOccurs = null)
        {
            MinOccurs = minOccurs;
            MaxOccurs = maxOccurs;
        }
    }


    public class Element : OccursElement
    {
        public string Name { get; private set; }

        public string Type { get; private set; }

        public Element(string name, string type, int? minOccurs = null, int? maxOccurs = null)
            : base(minOccurs, maxOccurs)
        {
            Name = name;
            Type = type;
        }
    }


    public class SequenceElement : OccursElement
    {
        public List<ElementBase> Elements { get; private set; } = new List<ElementBase>();

        public SequenceElement(int? minOccurs = null, int? maxOccurs = null)
            : base(minOccurs, maxOccurs)
        {
        }

        public SequenceElement Add(ElementBase element)
        {
            Elements.Add(element);
            return this;
        }

        public SequenceElement Add(IEnumerable<ElementBase> elements)
        {
            Elements.AddRange(elements);
            return this;
        }
    }


    public class ChoiceElement : SequenceElement
    {
        public ChoiceElement(int? minOccurs = null, int? maxOccurs = null)
            : base(minOccurs, maxOccurs)
        {
        }
    }


    public class AnyElement : OccursElement
    {
        public AnyElement(int? minOccurs = null, int? maxOccurs = null)
            : base(minOccurs, maxOccurs)
        {
        }
    }
}
