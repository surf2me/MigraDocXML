﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class DefaultTests
    {
        [Fact]
        public void SuccessfulDefault()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Default test=\"2\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Single(section.Children);
            Assert.IsType<Default>(section.Children.First());
            Assert.Equal(2, section.GetVariable("test"));
        }

        [Fact]
        public void UnsuccessfulDefault()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<Var test=\"5\"/>" +
                        "<Default test=\"2\"/>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();

            Assert.Equal(2, section.Children.Count());
            Assert.IsType<Var>(section.Children.First());
            Assert.IsType<Default>(section.Children.Last());
            Assert.Equal(5, section.GetVariable("test"));
        }
    }
}
