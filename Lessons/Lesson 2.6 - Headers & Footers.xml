<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section PageSetup.PageHeight="15cm">

        <Header IsPrimary="true">
            <p Format.Alignment="Right">Page <PageField/> of <NumPagesField/></p>
        </Header>

        <Footer IsPrimary="true">
            <p Format.Alignment="Center">This is part of the series of lessons for MigraDocXML</p>
        </Footer>

        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.6 - Headers &amp; Footers</p>

        <p>It's a very common requirement to have some common text showing at the top or bottom of each page within a document. MigraDoc includes both Header and Footer items to provide this control.</p>

        <p>In this lesson we'll go through 2 examples, the first (who's output you can see at the top &amp; bottom of this page) is quite a simple usage where we just have one header and one footer for each page. In the second example we still have one footer, but this time different headers depending on which page it's for.</p>

        <p Style="Code">
            &lt;Section&gt;
            {Space(4)}&lt;Header IsPrimary=&quot;true&quot;&gt;
                {Space(8)}&lt;p Format.Alignment=&quot;Right&quot;&gt;Page &lt;PageField/&gt; of &lt;NumPagesField/&gt;&lt;/p&gt;
            {Space(4)}&lt;/Header&gt;

            {Space(4)}&lt;Footer IsPrimary=&quot;true&quot;&gt;
                {Space(8)}&lt;p Format.Alignment=&quot;Center&quot;&gt;This is part of the series of lessons for MigraDocXML&lt;/p&gt;
            {Space(4)}&lt;/Footer&gt;
            {Space(4)}...
            
        </p>

        <p>Here we see that within the document section, we have one header and one footer defined. Each one sets the IsPrimary attribute to true, making it the default Header/Footer to use for the section. If these attributes hadn't been set then the Header &amp; Footer wouldn't appear.</p>

        <PageBreak/>

        <p>Notice that as we move to the next page, the Header &amp; Footer remain the same on each page.</p>

        <p>Also important to note are the <b>&lt;PageField/&gt;</b> and <b>&lt;NumPagesField/&gt;</b> elements within the header. These elements get evaluated at the rendering stage to return text values of the current page number and the total number of pages in the document respectively.</p>

        <p>Similar to these, there is also the <b>&lt;DateField/&gt;</b> element which returns a text representation with optional formatting of the date &amp; time at which the document was rendered.</p>

        <p>On the next page we have a more complex example.</p>
    </Section>

    <Section PageSetup.PageHeight="20cm" PageSetup.DifferentFirstPageHeaderFooter="true" PageSetup.OddAndEvenPagesHeaderFooter="true">

        <Header IsFirstPage="true">
            <Style Target="Paragraph">
                <Setters Format.Alignment="Right"/>
            </Style>
            
            <TextFrame RelativeHorizontal="Page" Left="Right" WrapFormat.DistanceRight="2.5cm" Width="3cm"
                       WrapFormat.DistanceTop="5mm" RelativeVertical="Page" Top="Top">
                <p>Created by:</p>
                <p Format.Font.Bold="true">James Coyle</p>
                <p Format.Font.Bold="true"><DateField Format="dd MMM yyyy"/></p>
            </TextFrame>
        </Header>

        <Header IsEvenPage="true">
            <p Format.Alignment="Right">Lesson 2.6 - Headers &amp; Footers</p>
        </Header>

        <Header IsPrimary="true">
            <p Format.Alignment="Right">Page <PageField/> of <NumPagesField/></p>
        </Header>

        <Footer IsPrimary="true" IsEvenPage="true" IsFirstPage="true">
            <p Format.Alignment="Center">This is part of the series of lessons for MigraDocXML</p>
        </Footer>

        <p>This example demonstrates how we can set different Headers &amp; Footers for different pages based on simple numbering rules.</p>

        <p Style="Code">
            &lt;Section PageSetup.DifferentFirstPageHeaderFooter=&quot;true&quot; 
                {Space(9)}PageSetup.OddAndEvenPagesHeaderFooter=&quot;true&quot;&gt;

                {Space(4)}&lt;Header IsFirstPage=&quot;true&quot;&gt;
                    {Space(8)}&lt;TextFrame Width=&quot;3cm&quot;
                                {Space(19)}RelativeHorizontal=&quot;Page&quot; Left=&quot;Right&quot; WrapFormat.DistanceRight=&quot;2.5cm&quot;
                                {Space(19)}RelativeVertical=&quot;Page&quot; Top=&quot;Top&quot; WrapFormat.DistanceTop=&quot;5mm&quot;&gt;
                        {Space(12)}&lt;p&gt;Created by:&lt;/p&gt;
                        {Space(12)}&lt;p Format.Font.Bold=&quot;true&quot;&gt;James Coyle&lt;/p&gt;
                        {Space(12)}&lt;p Format.Font.Bold=&quot;true&quot;&gt;&lt;DateField Format=&quot;dd MMM yyyy&quot;/&gt;&lt;/p&gt;
                    {Space(8)}&lt;/TextFrame&gt;
                {Space(4)}&lt;/Header&gt;

                {Space(4)}&lt;Header IsEvenPage=&quot;true&quot;&gt;
                    {Space(8)}&lt;p Format.Alignment=&quot;Right&quot;&gt;Lesson 2.6 - Headers &amp; Footers&lt;/p&gt;
                {Space(4)}&lt;/Header&gt;

                {Space(4)}&lt;Header IsPrimary=&quot;true&quot;&gt;
                    {Space(8)}&lt;p Format.Alignment=&quot;Right&quot;&gt;Page &lt;PageField/&gt; of &lt;NumPagesField/&gt;&lt;/p&gt;
                {Space(4)}&lt;/Header&gt;

                {Space(4)}&lt;Footer IsPrimary=&quot;true&quot; IsEvenPage=&quot;true&quot; IsFirstPage=&quot;true&quot;&gt;
                    {Space(8)}&lt;p Format.Alignment=&quot;Center&quot;&gt;This is part of the series of lessons for MigraDocXML&lt;/p&gt;
                {Space(4)}&lt;/Footer&gt;
                {Space(4)}...
        </p>

        <p>The rules for different Headers &amp; Footers are activated by setting attributes on the Section definition.</p>

        <list Type="BulletList1">
            <p><b>PageSetup.DifferentFirstPageHeaderFooter</b> allows you to set a special header/footer for the first page, as well as the primary one which is used as default</p>
            <p><b>PageSetup.OddAndEvenPagesHeaderFooter</b> allows you to set a special header/footer for evenly numbered pages, as well as the primary one which is used as default for the odd pages</p>
        </list>

        <p>In the above example we've allowed for a different first page, and different odd and even pages.</p>

        <p>The first header will be applied only to the first page of the section. The second header will be applied to all evenly numbered pages. Then the third header will be applied to all remaining pages which are neither the first page nor evenly numbered. The footer has been set up to appear on all pages of the section.</p>

        <PageBreak/>

        <PageBreak/>
        
    </Section>
</Document>