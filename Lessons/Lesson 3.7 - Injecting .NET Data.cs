﻿using MigraDocXML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    public static class Lesson3_7_Code
    {

        public static void Run()
        {
            //Build data model
            var order = new Order()
            {
                OrderNumber = "ORD1234",
                Date = new DateTime(2018, 08, 18, 16, 30, 0),
                Customer = new Customer()
                {
                    Name = "Bob & Sons Supermarket",
                    AccountNumber = 45927
                },
                OrderLines = new List<OrderLine>()
                {
                    new OrderLine()
                    {
                        Quantity = 1000,
                        Product = new Product()
                        {
                            Name = "Royal Gala Apple",
                            Price = 0.29M
                        }
                    },
                    new OrderLine()
                    {
                        Quantity = 500,
                        Product = new Product()
                        {
                            Name = "Brown Bread Loaf",
                            Price = 1.20M,
                            AdditionalData = new Dictionary<string, object>()
                            {
                                { "BatchNum", "BBL170818" },
                                { "BestBefore", new DateTime(2018, 9, 2) }
                            }
                        }
                    },
                    new OrderLine()
                    {
                        Quantity = 400,
                        Product = new Product()
                        {
                            Name = "Semi Skimmed Milk, 4 Pints",
                            Price = 1.00M,
                            AdditionalData = new Dictionary<string, object>()
                            {
                                { "BestBefore", new DateTime(2018, 8, 25) },
                                { "CountryOfOrigin", "Netherlands" }
                            }
                        }
                    }
                }
            };
            
            //Get the folder path where we'll be saving the lesson to
            string folderPath = typeof(Lesson3_7_Code).Assembly.Location;
            while (!folderPath.EndsWith("MigraDocXML"))
                folderPath = Directory.GetParent(folderPath).FullName;
            folderPath = Path.Combine(folderPath, "New Lessons");

            //Configure script access
            MigraDocScript.Settings.AllowMethodAccess = false;
            MigraDocScript.Settings.AllowPropertyGetterAccess = true;
            MigraDocScript.Settings.AllowStringIndexerAccess = true;

            //Generate PDF
            Document document = new PdfXmlReader(Path.Combine(folderPath, "Lesson 3.7 - Injecting .NET Data.xml")).Run(order);
            if (document == null)
                return;
            string outputFile = Path.Combine(folderPath, "Lesson 3.7 - Injecting .NET Data.pdf");
            MigraDoc.Rendering.PdfDocumentRenderer pdfRenderer = new MigraDoc.Rendering.PdfDocumentRenderer();
            pdfRenderer.Document = document.GetDocumentModel();
            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save(outputFile);
        }

    }


    public class Order
    {
        public string OrderNumber { get; set; }

        public DateTime Date { get; set; }

        public Customer Customer { get; set; }

        public List<OrderLine> OrderLines { get; set; }
    }


    public class Customer
    {
        public string Name { get; set; }

        public int AccountNumber { get; set; }
    }


    public class OrderLine
    {
        public decimal Quantity { get; set; }

        public Product Product { get; set; }
    }


    public class Product
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public Dictionary<string, object> AdditionalData { get; set; }
    }
}
