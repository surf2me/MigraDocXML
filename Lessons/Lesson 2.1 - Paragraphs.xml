<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 2.1 - Paragraphs</p>

        <p>The paragraph element is undoubtedly the most important element within all of MigraDoc. Every time you want to add text within your document, you'll need to use it.</p>

        <p>Usage is simple:</p>

        <p Style="Code">&lt;p&gt;Hello world!&lt;/p&gt;</p>

        <p>Gives the output:</p>

        <p>Hello world!</p>


        <p Style="SubTitle">Formatting</p>

        <p>Being able to write text to a document is fine and everything, but you probably want some control on how the text looks too. Luckily, the paragraph tag provides dozens of attributes for controlling the formatting of text. In this lesson I'll quickly go through the most common ones.</p>

        <Table Borders.Color="Black">
            <Var contentWidth="Section.PageSetup.ContentWidth"/>
            <Column Width="{contentWidth * 0.2}"/>
            <Column Width="{contentWidth * 0.6}"/>
            <Column Width="{contentWidth * 0.2}"/>

            <Style Target="C0">
                <Setters VerticalAlignment="Center"/>
            </Style>

            <Style Target="C1">
                <Setters VerticalAlignment="Center"/>
            </Style>

            <Row Heading="true" Format.Font.Bold="true" C0="Name" C1="Example" C2="Result"/>

            <Row C0="Horizontal Alignment">
                <C1>
                    <p Style="Code">&lt;p Format.Alignment=&quot;Right&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2 VerticalAlignment="Center">
                    <p Format.Alignment="Right">Hello world!</p>
                </C2>
            </Row>

            <Row C0="Bold">
                <C1>
                    <p Style="Code">&lt;p Format.Font.Bold=&quot;true&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.Font.Bold="true">Hello world!</p>
                </C2>
            </Row>
            
            <Row C0="Color">
                <C1>
                    <p Style="Code">&lt;p Format.Font.Color=&quot;Green&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.Font.Color="Green">Hello world!</p>
                </C2>
            </Row>

            <Row C0="Italics">
                <C1>
                    <p Style="Code">&lt;p Format.Font.Italic=&quot;true&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.Font.Italic="true">Hello world!</p>
                </C2>
            </Row>

            <Row C0="Font Name">
                <C1>
                    <p Style="Code">&lt;p Format.Font.Name=&quot;Comic Sans MS&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.Font.Name="Comic Sans MS">Hello world!</p>
                </C2>
            </Row>

            <Row C0="Font Size">
                <C1>
                    <p Style="Code">&lt;p Format.Font.Size=&quot;14&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.Font.Size="14">Hello world!</p>
                </C2>
            </Row>

            <Row C0="Space After">
                <C1>
                    <p Style="Code">&lt;p Format.SpaceAfter=&quot;6mm&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.SpaceAfter="6mm">Hello world!</p>
                </C2>
            </Row>

            <Row C0="Space Before">
                <C1>
                    <p Style="Code">&lt;p Format.SpaceBefore=&quot;6mm&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.SpaceBefore="6mm">Hello world!</p>
                </C2>
            </Row>
            
            <Row C0="Underline">
                <C1>
                    <p Style="Code">&lt;p Format.Font.Underline=&quot;Single&quot;&gt;Hello world!&lt;/p&gt;</p>
                </C1>
                <C2>
                    <p Format.Font.Underline="Single">Hello world!</p>
                </C2>
            </Row>
        </Table>


        <p Style="SubTitle">Partial Formatting</p>

        <p>You can also apply formatting to only part of a paragraph by using the <b>FormattedText</b> node, along with the various nodes that inherit from it.</p>

        <p>For example, if I want to set a key phrase in my paragraph to be large and red so it really stands out, I can do:</p>

        <p Style="Code">&lt;p&gt;You may &lt;FormattedText Font.Color="Red" Font.Size="12"&gt;NOT&lt;/FormattedText&gt; come in tomorrow!&lt;/p&gt;</p>

        <p>This produces:</p>

        <p>You may <FormattedText Font.Color="Red" Font.Size="12">NOT</FormattedText> come in tomorrow!</p>

        <p>Obviously, the downside of the above is that it's pretty long-winded and hard to read. For this reason, there are a number of shorthand nodes you can use for setting specific formatting options.</p>

        <p>For example, I can use <b>&lt;b&gt;&lt;/b&gt;</b> to bold part of my text, <i>&lt;i&gt;&lt;/i&gt;</i> to italicize phrases, <ul>&lt;ul&gt;&lt;/ul&gt;</ul> to underline sections, <sub>&lt;sub&gt;&lt;/sub&gt;</sub> to put bits in sub-script, and likewise <super>&lt;super&gt;&lt;/super&gt;</super> to set parts in super-script.
    </p>


        <p Style="SubTitle">Shorthand vs Longhand</p>

        <p>A small point to note, is that all of the examples in this document have actually been using the paragraph shorthand syntax. Technically the proper way to declare a paragraph is:</p>

        <p Style="Code">&lt;Paragraph&gt;Hello word!&lt;/Paragraph&gt;</p>

        <p>However, though both approaches give the same outputs, it's advisable to stick to using the shorthand approach, since this will make your designs far easier to read for almost anyone with any kind of HTML experience.</p>

        
        <p Style="SubTitle">Width</p>

        <p>Paragraphs behave much like they do in HTML whereby they take up as much horizontal space as can be afforded them by their parent. This is why the shading on the code samples in this document all stretch across the entire line, rather than ending where the text does.</p>

        <p>In the HTML world, this is what's referred to as a block element, however, unlike HTML, unfortunately MigraDoc provides no way to overwrite this behaviour, though it can be partially achieved using indents to manually limit the width of the paragraph:</p>

        <p Style="Code">&lt;p Format.Shading.Color="LightGreen" Format.RightIndent="14.4cm"&gt;Indent test&lt;/p&gt;</p>

        <p>Produces:</p>

        <p Format.Shading.Color="LightGreen" Format.RightIndent="14.4cm">Indent test</p>
    </Section>
</Document>